/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Damiam Kuznar
 * DATE:      26.07.2019
 *
 *******************************************************************************/

module mtm_Alu_core_tb ();

reg clk = 0,rst_n = 1,start = 0;
reg[31:0] data_A = 0,data_B = 0;
reg[2:0] op = 0;
reg err_data_in = 0,err_crc_in = 0;
wire[31:0] data_C;
wire[3:0] flags;
wire data_ready;
wire err_op_out,err_data_out,err_crc_out;


localparam[2:0] CRC_POLYNOMIAL = 3'b101;

mtm_Alu_core u_mtm_Alu_core(
    .clk(clk),
    .rst_n(rst_n),
    .data_A(data_A),
    .data_B(data_B),
    .op(op),
    .start(start),
    .err_data_in(err_data_in),
    .err_crc_in(err_crc_in),
    .data_C(data_C),
    .flags(flags),
    .data_ready(data_ready),
    .err_op_out(err_op_out),
    .err_data_out(err_data_out),
    .err_crc_out(err_crc_out)
);
reg[63:0] testVectors[511:0];
reg[31:0] i;
initial begin
    for(i=0;i<64;i=i+1) begin
        testVectors[i][31:0] = (i[0]?32'h3FFFFFFF:0) | ((i & 6)<<29);
        testVectors[i][63:32] = (i[3]?32'h3FFFFFFF:0) | ((i & 48)<<36);
    end
    for(i=64;i<512;i=i+1) begin
        testVectors[i][31:0] = $urandom;
        testVectors[i][63:32] = $urandom;
    end
end

always #10 clk = ~clk;

initial begin
	resetDevice();
    $display("--- Testing AND operation ---");
    testOperation(3'b000);
    $display("---        PASSED         ---");
    $display("--- Testing OR operation  ---");
    testOperation(3'b001);
    $display("---        PASSED         ---");
    $display("--- Testing ADD operation ---");
    testOperation(3'b100);
    $display("---        PASSED         ---");
    $display("--- Testing SUB operation ---");
    testOperation(3'b101);
    $display("---        PASSED         ---");
    $display("--- Testing ERR operation ---");
    testErrorOperation(3'b010);
    testErrorOperation(3'b011);
    testErrorOperation(3'b110);
    testErrorOperation(3'b111);
    $display("---        PASSED         ---");
    $display("-----------------------------");
    $display("---   ALL TESTS PASSED    ---");
    $display("-----------------------------");
    $stop;
end

task testOperation;
    input wire[2:0] op;
    integer num;
    reg[31:0] expResult;
    reg[3:0] expFlags;
    begin
        for(num=0;num<512;num=num+1) begin
            {expFlags,expResult} = calculateAluOp(testVectors[num][31:0],testVectors[num][63:32],op);
            setData(testVectors[num][31:0],testVectors[num][63:32],op);
            waitForResult(100);
            assertCorrect(expResult,expFlags);
            resetDevice();
        end
    end
endtask

task testErrorOperation;
    input wire[2:0] op;
    integer num;
    reg[31:0] expResult;
    reg[3:0] expFlags;
    begin
        for(num=0;num<512;num=num+1) begin
            setData(testVectors[num][31:0],testVectors[num][63:32],op);
            waitForResult(100);
            assertError();
            resetDevice();
        end
    end
endtask

function automatic[35:0] calculateAluOp(input[31:0] a,input[31:0] b,input[2:0] op);
    reg[31:0] expResult;
    reg[3:0] expFlags;
    begin
        expFlags = 0; //carry == 0
        case(op)
        3'b000: expResult = a & b;
        3'b001: expResult = a | b;
        3'b100: begin
            {expFlags[3],expResult} = a + b; //carry
            expFlags[2] = expFlags[3] ^ (a[31] ^ b[31]) ^ expResult[31]; //overflow
        end
        3'b101: begin
            {expFlags[3],expResult} = a - b; //carry
            expFlags[2] = expFlags[3] ^ (a[31] ^ b[31]) ^ expResult[31]; //overflow
        end
        default: expResult = 0;
        endcase
        expFlags[1] = expResult == 0; //zero
        expFlags[0] = expResult[31]; //negative
        calculateAluOp = {expFlags,expResult};
    end
endfunction

task assertCorrect;
    input wire[31:0] expectedResult;
    input wire[3:0] expectedFlags;
    begin
        verify(expectedResult,expectedFlags,0);
    end
endtask

task assertError;
    begin
        verify(0,0,1);
    end
endtask

task verify;
    input wire[31:0] expectedResult;
    input wire[3:0] expectedFlags;
    input wire expectedError;
    reg[36:0] tempData;
    reg[2:0] tempCrc;
    begin
        tempData = {expectedResult,expectedFlags};
        if(expectedError) begin
            if(~err_op_out) begin
                $display("Error: Module should set ERR_OP");
                $display(" Result: data_C = %d, flags = {%bC %bO %bZ %bN}",$signed(data_C),flags[3],flags[2],flags[1],flags[0]);
                $stop;
            end
        end else begin
            if(tempData != {data_C,flags} || err_op_out) begin
                $display("Error: Data does not match");
                $display(" Expected: data_C = %d, flags = {%bC %bO %bZ %bN}",
                $signed(expectedResult),expectedFlags[3],expectedFlags[2],expectedFlags[1],expectedFlags[0]);
                $display(" Actual:   data_C = %d, flags = {%bC %bO %bZ %bN}",
                $signed(data_C),flags[3],flags[2],flags[1],flags[0],err_op_out ? ", ERR_OP" : "");
                $stop;
            end
        end
    end
endtask

task setData;
    input wire[31:0] a;
    input wire[31:0] b;
    input wire[2:0] o;
    begin
        data_A = a;
        data_B = b;
        op = o;
        start = 1;
        @(negedge clk);
        data_A = 0;
        data_B = 0;
        op = 0;
        start = 0;
    end
endtask

task waitForResult;
    input integer timeout;
    integer i;
    begin
        if(~data_ready) begin
            for(i=timeout-1;i>=0;i=i-1) begin
                @(negedge clk);
                if(data_ready) i = 0;
            end
        end
        if(~data_ready) begin
            $display("Error: Timeout reached when waiting for ready flag");
            $stop;
        end
    end
endtask

task resetDevice;
    begin
        @(negedge clk);
        rst_n = 0;
        @(negedge clk);
        rst_n = 1;
    end
endtask

endmodule
