/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Przemysław Pawęski
 * DATE:      26.07.2019
 * 
 *******************************************************************************/

module mtm_Alu_deserializer_tb ();


  reg clk, rst, sin;
  reg [211:0] vectors[215:0];
  integer vectornum;
  wire [31:0] data_a, data_b;
  wire [2:0] op;
  wire ready, err_data, err_crc;
  reg hard_test_start;
  
mtm_Alu_deserializer u_mtm_Alu_deserializer(
    .clk(clk),
    .rst_n(rst),
    .sin(sin),
    .data_A(data_a),
    .data_B(data_b),
    .op(op), 
    .data_ready(ready),
    .err_data(err_data),
    .err_crc(err_crc)
);

initial begin
     // generate a clock
    forever #1 clk = ~clk;
end

initial begin
    $display("------------------------------------------------------------");
    $display("Deserializer test is starting");
    $display("------------------------------------------------------------");
    //initializaition
    $readmemb("mtm_Alu_deserializer.tv", vectors);
    vectornum = 0;
    
    reset();

    $display("Easy test - idle time between 11-bit frames and waiting for deserializer respond before next transmission");
    $display("------------------------------------------------------------");
    $display("Easy test - valid frames sending...");
    simple_test(50);
    $display("Test passed");
        
    $display("Easy test - crc error frames sending...");
    simple_test(50);
    $display("Test passed");
    
    $display("Easy test - data error frames sending...");
    simple_test(8);
    $display("Test passed");
        
    $display("Easy test - mixed type frames sending...");
    simple_test(108);
    $display("Test passed");
        
    reset();
    vectornum = 0;
    $display("------------------------------------------------------------");
    $display("Middle test - no idle time between 11-bit frames but waiting for deserializer respond before next transmission");
    $display("------------------------------------------------------------");
    $display("Middle test - valid frames sending...");
    simple_test_fast(50);
    $display("Test passed");
        
    $display("Middle test - crc error frames sending...");
    simple_test_fast(50);
    $display("Test passed");
       
    $display("Middle test - data error frames sending...");
    simple_test_fast(8);
    $display("Test passed");
         
    $display("Middle test - mixed type frames sending...");
    simple_test_fast(108);
    $display("Test passed");
    reset();
    hard_test_start = 1'b1; //go to hard test
end

initial begin
    hard_test_start = 1'b0;
    @ (posedge hard_test_start); //wait for middle test end
    $display("------------------------------------------------------------");
    $display("Hard test - all frames sending without waiting");
    $display("------------------------------------------------------------");
    $display("Hard test - sending all frames");
    send_all_vectors();
end

initial begin
    hard_test_start = 1'b0;
    @ (posedge hard_test_start); //wait for middle test end

    compare_all_vectors();
    
    
    
    $display("------------------------------------------------------------");
    $display("All tests passed - test complete");
    $display("------------------------------------------------------------");
    $stop;
end

task send_byte;
    input [10:0] byte;
    integer i;
    begin
        if( byte != 0)
            for (i=10; i >= 0; i = i -1) begin
                @ (negedge clk);
                sin = byte[i];
            end
    end 
endtask

task send_vector;
    input integer vector_number;
    reg [211:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        repeat(13)begin
            repeat(4) @(negedge clk); //idle time between frames
            send_byte(current_vector[211:201]); //send frame
            current_vector = {current_vector[200:0], 11'b0};
        end
    end
endtask

task send_vector_no_delay;
    input integer vector_number;
    reg [211:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        repeat(13)begin
            send_byte(current_vector[211:201]); //send frame
            current_vector = {current_vector[200:0], 11'b0};
        end
    end
endtask

task send_all_vectors;
    begin
        vectornum = 0;
        repeat(216) begin
            send_vector_no_delay(vectornum);
            vectornum = vectornum + 1;
        end
    end
endtask

task simple_test;
    input integer vectors_to_send;
    reg error;
    begin
        repeat(vectors_to_send) begin
            send_vector(vectornum);
            compare(vectornum, error);
            if(error == 1'b1) begin
                $display("Test Failed - stopping");
                $stop;
            end
            vectornum = vectornum + 1;
        end
    end
endtask

task simple_test_fast;
    input integer vectors_to_send;
    reg error;
    begin
        repeat(vectors_to_send) begin
            send_vector_no_delay(vectornum);
            compare(vectornum, error);
            if(error == 1'b1) begin
                $display("Test Failed - stopping");
                $stop;
            end
            vectornum = vectornum + 1;
        end
    end
endtask

task compare;
    input integer vector_number;
    output status; // 0 - OK, 1 - error
    reg [31:0] a_expected;
    reg [31:0] b_expected;
    reg [2:0] op_expected;
    reg [1:0] vector_type;
    integer timeout;
    reg [211:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        timeout = 200; // 100 - cycles to wait, 1- timeout, 0 - no timeout
        b_expected = current_vector[68:37];
        a_expected = current_vector[36:5];
        op_expected = current_vector[4:2];
        vector_type = current_vector[1:0];
        //wait for respond
        while(timeout > 1) begin
            @(posedge clk);
            if(ready == 1'b1)
                timeout = 0;
            else
                timeout = timeout - 1;
        end
        
        case(vector_type)
        2'b00 : //valid data vector
            if( timeout == 1'b1 )begin
                $display("Test failed at valid vector number %d - deserializer respond timeout",vector_number);
                status = 1'b1;
            end
            else if( err_data == 1'b1) begin
                $display("Test failed at valid vector number %d - unexpected data error at deserializer output",vector_number);
                status = 1'b1;
            end
            else if( err_crc == 1'b1) begin
                $display("Test failed at valid vector number %d - unexpected crc error at deserializer output",vector_number);
                status = 1'b1;
            end
            else if( (data_a != a_expected) | (data_b != b_expected) | (op != op_expected) )begin
                $display("Test failed at valid vector number %d - incorrect data at deserializer output",vector_number);
                $display("A_received: %d, A_expected: %d",data_a,a_expected);
                $display("B_received: %d, B_expected: %d",data_b,b_expected);
                $display("op_received: %d, op_expected: %d",op,op_expected);
                status = 1'b1;
            end
            else
                status = 1'b0;
        2'b01 : //crc error vector
            if( timeout == 1'b1 )begin
                $display("Test failed at err_crc vector number %d - deserializer respond timeout",vector_number);
                status = 1'b1;
            end
            else if( err_data == 1'b1) begin
                $display("Test failed at err_crc vector number %d - unexpected data error at deserializer output",vector_number);
                status = 1'b1;
            end
            else if( err_data == 1'b1) begin
                $display("Test failed at err_crc vector number %d - deserializer didn't set the crc error flag for vrctor with crc error",vector_number);
                status = 1'b1;
            end
            else
                status = 1'b0;
        2'b10 : //data error vector
            if( timeout == 1'b1 )begin
                $display("Test failed at err_data vector number %d - deserializer respond timeout",vector_number);
                status = 1'b1;
           end
            else if( err_data == 1'b1 & err_crc == 1'b1) begin
                $display("Test failed at err_data vector number %d - error data flag set, but also crc error flag set",vector_number);
                status = 1'b1;
          end
          else if( err_data == 1'b0 & err_crc == 1'b1) begin
                $display("Test failed at err_data vector number %d - only crc flag set",vector_number);
                 status = 1'b1;
           end
          else if( err_data == 1'b0) begin
                $display("Test failed at err_data vector number %d - deserializer didn't set data error flag for vector with corresponding error",vector_number);
                status = 1'b1;
            end
            else
                status = 1'b0;
        default: begin
            status = 1'b1;
            $display("Error - unknown vector type. Vector number: %d",vector_number);
        end
        endcase
        
    end
endtask

task compare_all_vectors;
    integer vector_number;
    reg error;
    begin
        vector_number = 0;
        error = 1'b0;
        repeat(216) begin
            compare(vector_number, error);
            if( error == 1'b1 ) begin
                $display("Test Failed - stopping");
                $stop;
            end
            vector_number = vector_number + 1;
        end
    end
endtask

task reset;
begin
    sin = 1'b1;
    clk = 1'b0;
    rst = 1'b0;
    repeat(2) @(negedge clk);
    rst = 1'b1;
    repeat(2) @(negedge clk);
end
endtask
  
endmodule
