/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    serializer
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Przemysław Pawęski
 * DATE:      26.07.2019
 *
 *******************************************************************************/

`define FIFO_SIZE 2000

module mtm_Alu_serializer_tb ();

reg clk, rst, start;
wire sout;
reg [31:0] Data_in;
reg [3:0] flags;
reg err_data, err_crc, err_op;
reg [10:0] current_frame;
reg [93:0] vectors[118:1];
integer vectornum;
reg err;
reg start_read;
integer read_ctr;


reg [10:0] fifo_frames_mem [`FIFO_SIZE-1:0];
integer fifo_frames_write_ctr = 0;
integer fifo_frames_read_ctr = 0;

mtm_Alu_serializer u_mtm_Alu_serializer(
    .clk(clk),
    .rst_n(rst),
    .start(start),
    .data_C(Data_in),
    .flags(flags),
    .err_data(err_data),
    .err_crc(err_crc),
    .err_op(err_op),
    .sout(sout) 
);

initial begin
     // generate a clock
    forever #1 clk = ~clk;
end

initial begin
    $display("------------------------------------------------------------");
    $display("Serializer test is starting");
    $display("------------------------------------------------------------");
    //initializaition
    $readmemb("mtm_Alu_serializer.tv", vectors);
    vectornum = 1;
    start_read = 1'b0;
    
    reset();
    clear_fifo();
    
    if(sout == 1'b0) begin
        $display("Test Failed - sout should be 1'b1 after reset - stopping");
        $stop;
    end
    
    $display("Easy test - idle time time been next request.");
    $display("------------------------------------------------------------");
    $display("Easy test - valid frames sending...");
    simple_test(50);
    $display("Test passed");
    
    $display("Easy test - error frames sending...");
    simple_test(3);
    $display("Test passed");
    
    $display("Easy test - mixed types frames sending...");
    simple_test(65);
    $display("Test passed");
    
    vectornum = 1;
    $display("Hard test - sending all frames again with appropirate delays .");
    $display("------------------------------------------------------------");
    $display("Hard test - sending all vectors");
    send_all_vectors(118);
    $display("Hard test - verification...");
    repeat(150)
            @(negedge clk);
    compare_all_frames(118);
    $display("Test passed");
    $display("Test complete");
    $stop;
end

task simple_test;
    input integer vectors_to_test;
    begin
        repeat(vectors_to_test) begin
            send_vector(vectornum);
            repeat(100) begin
                @(posedge clk);
            end
            compare(vectornum,err);
            if(err == 1'b1)begin
                $display("Test Failed - stopping");
                $stop;
            end
            vectornum = vectornum +1;
        end    
    end
endtask


task send_all_vectors;
    input integer vectors_to_test;
    begin
        repeat(vectors_to_test) begin
            simulate_deserializer_delay(vectornum);
            send_vector(vectornum);
            vectornum = vectornum +1;
        end    
    end
endtask

task compare_all_frames;
    input integer vectors_to_test;
    integer ctr;
    begin
        ctr = 1;
        repeat(vectors_to_test) begin
            compare_hard(ctr,err);
            if(err == 1'b1)begin
                $display("Test Failed - stopping");
                $stop;
            end
            ctr = ctr +1;
        end    
    end
endtask

task simulate_deserializer_delay;
    input integer vector_number;
    reg [93:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        if( current_vector[57:55]==0 )begin // valid frame
            repeat(98)
                @(negedge clk);
        end
        else if (current_vector[55]==1'b1)begin // OP error
            repeat(98)
                @(negedge clk);
        end
        else if (current_vector[56]==1'b1)begin // CRC error
            repeat(98)
                @(negedge clk);
        end  
        else if (current_vector[57]==1'b1)begin // data error
            repeat(12)
                @(negedge clk);
        end  
    end
endtask

//reading from sout
always@ (negedge clk) begin
    if(start_read == 1'b1)begin
        read_ctr = read_ctr -1;
        current_frame[read_ctr] = sout;
        if(read_ctr == 0)begin
            start_read = 1'b0;
            fifo_add_frame(current_frame);
        end
    end
    else begin
        if( sout == 1'b0) begin
            start_read = 1'b1;
            current_frame = 11'b00000000000;
            read_ctr = 10;
        end
    end
end

task reset;
begin
    clk = 1'b0;
    start = 1'b0;
    rst = 1'b0;
    repeat(2) @(negedge clk);
    rst = 1'b1;
    repeat(2) @(negedge clk);
end
endtask

task compare;
    input integer vector_number;
    output status;
    reg [93:0] current_vector;
    integer received_frames_number;
    reg[54:0] received_frames;
    begin
        current_vector = vectors[vector_number];
        fifo_get_frames_number(received_frames_number);
        if( current_vector[57:55]==0 )begin // valid frames
            
            if( received_frames_number == 0 )begin
                $display("Test failed at valid vector number %d - serializer respond timeout",vector_number);
                status = 1'b1;
            end 
            else if ( received_frames_number != 5 )begin
                $display("Test failed at valid vector number %d - received %d frames instead of 5",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[54:44]);
                fifo_get_frame(received_frames[43:33]);
                fifo_get_frame(received_frames[32:22]);
                fifo_get_frame(received_frames[21:11]);
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames != current_vector[54:0] )begin
                    $display("Test failed at valid vector number %d - incorrenct frames",vector_number);
                    $display("Received: %b",received_frames);
                    $display("Expected: %b",current_vector[54:0]);
                    status = 1'b1;
                end
                else
                    status = 1'b0;
            end
            
        end 
        else begin // error frames
            if( received_frames_number == 0 )begin
                $display("Test failed at error vector number %d - serializer respond timeout",vector_number);
                status = 1'b1;
            end 
            else if ( received_frames_number != 1 )begin
                $display("Test failed at error vector number %d - received %d frames instead of 1",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames[10:0] != current_vector[54:44] )begin
                    $display("Test failed at error vector number %d - incorrenct frame",vector_number);
                    $display("Received: %b",received_frames[10:0]);
                    $display("Expected: %b",current_vector[54:44]);
                    status = 1'b1;
                    end
                else
                    status = 1'b0;
                end
        end
    end
endtask

task compare_hard;
    input integer vector_number;
    output status;
    reg [93:0] current_vector;
    integer received_frames_number;
    reg[54:0] received_frames;
    begin
        current_vector = vectors[vector_number];
        fifo_get_frames_number(received_frames_number);
        if( current_vector[57:55]==0 )begin // valid frames
            
            if ( received_frames_number < 5 )begin
                $display("Test failed at valid vector number %d - at this point there should be at least 5 frames in fifo, but there are %d instead",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[54:44]);
                fifo_get_frame(received_frames[43:33]);
                fifo_get_frame(received_frames[32:22]);
                fifo_get_frame(received_frames[21:11]);
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames != current_vector[54:0] )begin
                    $display("Test failed at valid vector number %d - incorrenct frames",vector_number);
                    $display("Received: %b",received_frames);
                    $display("Expected: %b",current_vector[54:0]);
                    status = 1'b1;
                end
                else
                    status = 1'b0;
            end
            
        end 
        else begin // error frames
            if ( received_frames_number < 1 )begin
                $display("Test failed at error vector number %d - at this point there should be at least 1 frame in fifo, but there are %d instead",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames[10:0] != current_vector[54:44] )begin
                    $display("Test failed at error vector number %d - incorrenct frame",vector_number);
                    $display("Received: %b",received_frames[10:0]);
                    $display("Expected: %b",current_vector[54:44]);
                    status = 1'b1;
                    end
                else
                    status = 1'b0;
                end
        end
    end
endtask


task send_vector;
    input integer vector_number;
    reg [93:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        @(negedge clk);   
        Data_in = current_vector[93:62];
        flags = current_vector[61:58];
        err_data = current_vector[57];
        err_crc = current_vector[56];
        err_op = current_vector[55];
        start = 1'b1;
        @(negedge clk);
        start = 1'b0;
    end
endtask

task clear_fifo;
    integer ctr;
    begin
        ctr = 0;
        repeat(`FIFO_SIZE)begin
            fifo_frames_mem[ctr] = 0;
            ctr = ctr +1;
            fifo_frames_write_ctr = 0;
            fifo_frames_read_ctr = 0;
        end
    end
endtask

task fifo_add_frame;
    input [10:0] new_frame;
    begin
        fifo_frames_mem[fifo_frames_write_ctr] = new_frame;
        fifo_frames_write_ctr = fifo_frames_write_ctr +1;
    end
endtask

task fifo_get_frame;
    output [10:0] new_frame;
    begin
        new_frame = fifo_frames_mem[fifo_frames_read_ctr];
        fifo_frames_read_ctr = fifo_frames_read_ctr +1;
    end
endtask

task fifo_check_if_empty;
    output empty;
    if(fifo_frames_write_ctr > fifo_frames_read_ctr)
        empty = 1'b0;
    else
        empty = 1'b1;
endtask

task fifo_get_frames_number;
    output integer frames_number;
    if(fifo_frames_write_ctr >= fifo_frames_read_ctr)
        frames_number = fifo_frames_write_ctr - fifo_frames_read_ctr;
    else
        frames_number = 999999;
endtask

endmodule

