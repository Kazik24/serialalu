/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu tb
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Przemysław Pawęski
 * DATE:      26.07.2019
 * ------------------------------------------------------------------------------
 * This module (TB) provides test patterns for the ALU, reads data from the ALU and 
 * verifies if the operation result is correct.
 * 
 * The TB must include:
 * - task send_byte to send a CMD or CTL command to the ALU
 * - task send_calculation_data that will send 9 bytes to the ALU for given
 *   operands and operation
 * - procedural block for capturing the input data from the ALU
 * - task compare to compare the result from the ALU and the expected data.
 * 
 * The test vectors must provide at least:
 * - sending max (0xFFFF) and min (0) data with all the ALU operations (AND OR, ADD,SUB)
 * - sending 1000 random valid data
 * - sending invalid data (wrong number of DATA packets before CTL packet)
 * - sending data with CRC error
 * 
 * The testbench should print final PASS/FAIL text information.
 */
 
`define FIFO_SIZE 10000

module mtm_Alu_tb (
    output reg clk,
    output reg rst_n,
    output reg sin,
    input wire sout
);

integer vectornum;
reg [155:0] vectors[1510:1];
reg start_read;
integer read_ctr;
reg [10:0] current_frame;

//fifo for received frames
reg [10:0] fifo_frames_mem [`FIFO_SIZE-1:0];
integer fifo_frames_write_ctr = 0;
integer fifo_frames_read_ctr = 0;

initial begin
     // generate a clock
    forever #10 clk = ~clk;
end

initial begin
    $display("------------------------------------------------------------");
    $display("ALU test is starting");
    $display("------------------------------------------------------------");
    $display("In this test frames will be send as fast as possible");
    $display("------------------------------------------------------------");
    //initializaition
`ifdef TB_DIR
    $readmemb({`TB_DIR,"/mtm_Alu.tv"}, vectors);
`else
    $readmemb("mtm_Alu.tv", vectors);
`endif
    vectornum = 1;
    start_read = 1'b0;
    
    reset();
    clear_fifo();
    
    $display("Sending min-max data for all ALU operations (vectors 1-16)");
    repeat(16) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    $display("Sending 300 valid data for all ALU operations (vectors 17-1216)");
    repeat(1200) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    $display("Sending CRC error data  (vectors 1217-1226)");
    repeat(10) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    
    $display("Sending OP error data  (vectors 1227-1246)");
    repeat(20) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    
    $display("Sending frame number error data  (vectors 1247-1258)");
    repeat(12) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    
    $display("Sending mixed type frames  (vectors 1259-1510)");
    repeat(252) begin
        send_calculation_data(vectornum);
        vectornum = vectornum + 1;
    end
    
    $display("------------------------------------------------------------");
    $display("Verification...");
    $display("------------------------------------------------------------");
    
    repeat(150)
        @(negedge clk);
    compare_all(1510); 
    $display("Test passed :D");
    $stop;
    
end

//reading from sout
always@ (negedge clk) begin
    if(start_read == 1'b1)begin
        read_ctr = read_ctr -1;
        current_frame[read_ctr] = sout;
        if(read_ctr == 0)begin
            start_read = 1'b0;
            fifo_add_frame(current_frame);
        end
    end
    else begin
        if( sout == 1'b0) begin
            start_read = 1'b1;
            current_frame = 11'b00000000000;
            read_ctr = 10;
        end
    end
end

task reset;
begin
    sin = 1'b1;
    clk = 1'b0;
    rst_n = 1'b0;
    repeat(2) @(negedge clk);
    rst_n = 1'b1;
    repeat(2) @(negedge clk);
end
endtask

task compare_all;
    input integer frames_number;
    integer ctr;
    reg err;
    begin
        ctr = 1;
        err = 1'b0;
        repeat(frames_number)begin
            compare(ctr,err);
            if(err == 1'b1)begin
                $display("------------------------------------------------------------");
                $display("Test Failed - stopping");
                $stop;
            end
            ctr = ctr + 1;
        end
    end
endtask

task compare();
    input integer vector_number;
    output status;
    reg [155:0] current_vector;
    integer received_frames_number;
    reg[54:0] received_frames;
    begin
        current_vector = vectors[vector_number];
        fifo_get_frames_number(received_frames_number);
        if( current_vector[56:55]==0 )begin // valid frames
            
            if ( received_frames_number < 5 )begin
                $display("Test failed at valid vector number %d - at this point there should be at least 5 frames in fifo, but there are %d instead",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[54:44]);
                fifo_get_frame(received_frames[43:33]);
                fifo_get_frame(received_frames[32:22]);
                fifo_get_frame(received_frames[21:11]);
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames != current_vector[54:0] )begin
                    $display("Test failed at valid vector number %d - incorrenct frames",vector_number);
                    $display("Received: %b",received_frames);
                    $display("Expected: %b",current_vector[54:0]);
                    status = 1'b1;
                end
                else
                    status = 1'b0;
            end
            
        end 
        else begin // error frames
            if ( received_frames_number < 1 )begin
                $display("Test failed at error vector number %d - at this point there should be at least 1 frame in fifo, but there are %d instead",vector_number, received_frames_number);
                status = 1'b1;
            end
            else begin
                fifo_get_frame(received_frames[10:0]);
                if ( received_frames[10:0] != current_vector[54:44] )begin
                    $display("Test failed at error vector number %d - incorrenct frame",vector_number);
                    $display("Received: %b",received_frames[10:0]);
                    $display("Expected: %b",current_vector[54:44]);
                    status = 1'b1;
                    end
                else
                    status = 1'b0;
                end
        end
    end
endtask

task send_calculation_data;
    input integer vector_number;
    reg [155:0] current_vector;
    begin
        current_vector = vectors[vector_number];
        repeat(9) begin
            send_byte(current_vector[155:145]);
            current_vector = {current_vector[144:0], 11'b00000000000};
        end
    end
endtask

task send_byte;
    input [10:0] byte;
    integer i;
    begin
        if( byte != 0)
            for (i=10; i >= 0; i = i -1) begin
                @ (negedge clk);
                sin = byte[i];
            end
    end 
endtask

task clear_fifo;
    integer ctr;
    begin
        ctr = 0;
        repeat(`FIFO_SIZE)begin
            fifo_frames_mem[ctr] = 0;
            ctr = ctr +1;
            fifo_frames_write_ctr = 0;
            fifo_frames_read_ctr = 0;
        end
    end
endtask

task fifo_add_frame;
    input [10:0] new_frame;
    begin
        fifo_frames_mem[fifo_frames_write_ctr] = new_frame;
        fifo_frames_write_ctr = fifo_frames_write_ctr +1;
    end
endtask

task fifo_get_frame;
    output [10:0] new_frame;
    begin
        new_frame = fifo_frames_mem[fifo_frames_read_ctr];
        fifo_frames_read_ctr = fifo_frames_read_ctr +1;
    end
endtask

task fifo_check_if_empty;
    output empty;
    if(fifo_frames_write_ctr > fifo_frames_read_ctr)
        empty = 1'b0;
    else
        empty = 1'b1;
endtask

task fifo_get_frames_number;
    output integer frames_number;
    if(fifo_frames_write_ctr >= fifo_frames_read_ctr)
        frames_number = fifo_frames_write_ctr - fifo_frames_read_ctr;
    else
        frames_number = 999999;
endtask

endmodule
