/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Damian Kuznar
 * DATE:      17.08.2019
 * 
 *******************************************************************************/

module mtm_Alu_crc#(
    parameter N = 4,
    //N bit polynomial part, acts as N+1 bit value with MSB set to 1
    parameter CRC_POLYNOMIAL = 4'b0110
)(
    input wire clk,
    input wire rst,
    input wire enable,
    input wire data_bit,
    output reg[N-1:0] crc
);



reg[N-1:0] crc_nxt;

always @* begin
    //shift left
    crc_nxt = {crc[N-2:0],data_bit};
    
    //if last bit is of current state is 1, it means that i will be cleared by
    //5 bit of polynomial which is allways 1
    if(crc[N-1]) begin
            //xor shifted value with polynomial
            crc_nxt = crc_nxt ^ CRC_POLYNOMIAL;
    end
end

always @(posedge clk) begin
    if(rst) crc <= 0;
    else if(enable) crc <= crc_nxt;
end

endmodule
