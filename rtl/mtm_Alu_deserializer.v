/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Damian Kuznar
 * DATE:      26.07.2019
 * 
 *******************************************************************************/

module mtm_Alu_deserializer (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire sin,   // serial data input
    output reg [31:0] data_A,
    output reg [31:0] data_B,
    output reg [2:0] op, 
    output reg data_ready,
    output reg err_data,
    output reg err_crc
);

localparam CRC_POLYNOMIAL = 4'b0011;

reg crcBit,shift,appendCrc;
reg err_data_nxt,err_crc_nxt,data_ready_nxt;
//bits shift from right to left
reg[71:0] shiftPayload,shiftPayload_nxt; // final state{B[31:0], A[31:0], 1'b0, OP[2:0], CRC[3:0]}
wire[3:0] receivedCrc;
assign receivedCrc = shiftPayload[3:0];
reg[3:0] dataPackCounter,dataPackCounter_nxt;
reg[3:0] bitInPackCounter,bitInPackCounter_nxt;
reg[31:0] data_A_nxt,data_B_nxt;
reg[2:0] op_nxt;

//flags for current packet receiving state
wire bitZero,bitOne,bitTwo,bitData,bitDataUpper,bitStop,lastPacket,shouldBeCommand;
assign bitZero = bitInPackCounter == 0;
assign bitOne = bitInPackCounter == 1;
assign bitTwo = bitInPackCounter == 2;
assign bitData = bitInPackCounter > 1 && bitInPackCounter < 10;
assign bitDataUpper = bitInPackCounter > 5;
assign lastPacket = dataPackCounter[3] && dataPackCounter[0]; // simplified dataPackCounter == 9
assign shouldBeCommand = dataPackCounter[3] && ~dataPackCounter[0];  // simplified dataPackCounter == 8

//crc module
wire[3:0] calculatedCrc;
mtm_Alu_crc#(4,CRC_POLYNOMIAL) crc(
    .clk(clk),
    .rst(data_ready_nxt || ~rst_n),
    .enable(appendCrc),
    .data_bit(crcBit),
    .crc(calculatedCrc)
);

always @* begin
    crcBit = sin;
    data_ready_nxt = 0;//clear it right away cause it will be only high during 1 clock tick
    err_data_nxt = err_data;//accumulated data cause we need it at the end of packet
    err_crc_nxt = 0;//clear it right away cause it will be only high during 1 clock tick
    dataPackCounter_nxt = dataPackCounter;
    //defaults for flip flop enable signals
    shiftPayload_nxt = shiftPayload;
    appendCrc = 0;
    data_B_nxt = data_B;
    data_A_nxt = data_A;
    op_nxt = op;
    
    if(bitZero) begin
        err_data_nxt = 0; //reset accumulated data error
        //increment counter only if start bit is detected
        bitInPackCounter_nxt = {3'b000,~sin};
    end else if(bitOne) begin
        bitInPackCounter_nxt = 2;
        if(sin) begin // command packet
            if(shouldBeCommand) begin
                dataPackCounter_nxt = 9; // indicate that this is last packet, should be cleared to 0 after it
            end else begin
                err_data_nxt = 1; // data error, less than 8 data packets before command
            end
        end else begin // data packet
            if(shouldBeCommand) begin
                err_data_nxt = 1; // data error, more data packets than 8
            end else begin
                dataPackCounter_nxt = dataPackCounter + 1;
            end
        end
    end else if(bitData) begin //data bits
        shiftPayload_nxt = {shiftPayload[70:0], sin};
        appendCrc = 1; //append data to crc
        bitInPackCounter_nxt = bitInPackCounter + 1;
        if(lastPacket) begin
            if(bitTwo) begin
                //that bit before op part in cmd packet
                //negated cause we then we should detect crc error if this bit will be 1 instead of 0 in packet
                //we want crc calculated for it like it will be 1 in correct case
                crcBit = ~sin;
            end else if(bitDataUpper) begin //data bits 
                crcBit = 0;//append 0 to crc, we only need to calculate its result here
            end
        end
        
    end else begin //stop bit 1'b1
        bitInPackCounter_nxt = 0;
        err_data_nxt = err_data_nxt || ~sin;//if no stop bit then raise error
        if(lastPacket) begin // last packet finished
            //compare crc and set error if not same
            if(receivedCrc != calculatedCrc) err_crc_nxt = 1;
            data_ready_nxt = 1;
            dataPackCounter_nxt = 0;
            shiftPayload_nxt = 0;
        end
        //if data error in this packet then finish whole receive sequence
        if(err_data_nxt) begin
            data_ready_nxt = 1;
            dataPackCounter_nxt = 0;
            shiftPayload_nxt = 0;
        end
        //set outputs
        if(data_ready_nxt) begin
            data_B_nxt = shiftPayload[71:40];
            data_A_nxt = shiftPayload[39:8];
            op_nxt = shiftPayload[6:4];
        end
    end
    
end

always @(posedge clk) begin
    if(~rst_n) begin
        //reset outputs
        data_ready <= 0;
        err_data <= 0;
        err_crc <= 0;
        data_B <= 0;
        data_A <= 0;
        op <= 0;
        //reset internal state
        shiftPayload <= 0;
        bitInPackCounter <= 0;
        dataPackCounter <= 0;
    end else begin
        //outputs
        data_ready <= data_ready_nxt;
        err_data <= err_data_nxt;
        err_crc <= err_crc_nxt;
        data_B <= data_B_nxt;
        data_A <= data_A_nxt;
        op <= op_nxt;
        
        //internal state
        shiftPayload <= shiftPayload_nxt;
        bitInPackCounter <= bitInPackCounter_nxt;
        dataPackCounter <= dataPackCounter_nxt;
    end
end

endmodule
