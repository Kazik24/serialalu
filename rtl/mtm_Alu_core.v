/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Przemyslaw Paweski
 * DATE:      26.07.2019
 *
 *******************************************************************************/

module mtm_Alu_core (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire [31:0] data_A,
    input  wire [31:0] data_B,
    input  wire [2:0] op,
    input  wire start,
    input  wire err_data_in,
    input  wire err_crc_in,
    output reg [32:0] data_C,
    output reg [3:0] flags,
    output wire data_ready,
    output reg err_op_out,
    output reg err_data_out,
    output reg err_crc_out
);

reg [31:0] A_tmp,B_tmp;
reg [2:0] op_tmp;
reg [32:0] data_C_nxt;
reg [2:0] carry, carry_nxt;
reg err_op_out_nxt;
reg [5:0] ready_buff, ready_buff_nxt;


reg overflow_flag_nxt, carry_flag_nxt, zero_flag_nxt, negative_flag_nxt;

always @(posedge clk)
    if(rst_n) begin
        carry <= carry_nxt;
        data_C <= data_C_nxt[32:0];
        if( start == 1'b1 )begin
            A_tmp <= data_A;
            B_tmp <= data_B;
            op_tmp <= op;
            err_data_out <= err_data_in;
            err_crc_out <= err_crc_in;
        end
        else begin
            A_tmp <= A_tmp;
            B_tmp <= B_tmp;
            op_tmp <= op_tmp;
            err_data_out <= err_data_out;
            err_crc_out <= err_crc_out;
        end
        ready_buff <= ready_buff_nxt;
        err_op_out <= err_op_out_nxt;
        flags <= {carry_flag_nxt, overflow_flag_nxt, zero_flag_nxt, negative_flag_nxt};
    end
    else begin
        ready_buff <= 6'b0;
    end
    
assign data_ready = ready_buff[5];

always @(*) begin
    ready_buff_nxt <= {ready_buff[4:0], start};
    
    case (op_tmp)
    //ADD
    3'b100 : begin
        {carry_nxt[0], data_C_nxt[7:0]} = A_tmp[7:0] + B_tmp[7:0];
        {carry_nxt[1], data_C_nxt[15:8]} = A_tmp[15:8] + B_tmp[15:8] + carry[0];
        {carry_nxt[2], data_C_nxt[23:16]} = A_tmp[23:16] + B_tmp[23:16] + carry[1];
        data_C_nxt[32:24] = A_tmp[31:24] + B_tmp[31:24] + carry[2];
        
        overflow_flag_nxt = data_C[32] ^ ((A_tmp[31] ^ B_tmp[31]) ^ data_C[31]);
        carry_flag_nxt = data_C[32];
        err_op_out_nxt = 1'b0;
    end
    //SUB
    3'b101 : begin
        {carry_nxt[0], data_C_nxt[7:0]} = A_tmp[7:0] - B_tmp[7:0];
        {carry_nxt[1], data_C_nxt[15:8]} = A_tmp[15:8] - B_tmp[15:8] - carry[0];
        {carry_nxt[2], data_C_nxt[23:16]} = A_tmp[23:16] - B_tmp[23:16] - carry[1];
        data_C_nxt[32:24] = A_tmp[31:24] - B_tmp[31:24] - carry[2];
        
        overflow_flag_nxt = data_C[32] ^ ((A_tmp[31] ^ B_tmp[31]) ^ data_C[31]);
        carry_flag_nxt = data_C[32];
        err_op_out_nxt = 1'b0;
    end
    //AND
    3'b000 : begin
        data_C_nxt = {1'b0, A_tmp & B_tmp};
        carry_nxt = carry;
        
        overflow_flag_nxt = 1'b0;
        carry_flag_nxt = 1'b0;
        err_op_out_nxt = 1'b0;
    end
    //OR
    3'b001 : begin
        data_C_nxt = {1'b0, A_tmp | B_tmp};
        carry_nxt = carry;
        
        overflow_flag_nxt = 1'b0;
        carry_flag_nxt = 1'b0;
        err_op_out_nxt = 1'b0;
    end
    default : begin
        data_C_nxt = data_C;
        carry_nxt = carry;
        
        overflow_flag_nxt = 1'b0;
        carry_flag_nxt = 1'b0;
        if( ~(err_data_out | err_crc_out))
            err_op_out_nxt = 1'b1;
        else
            err_op_out_nxt = 1'b0;
    end
    endcase
    
    if(data_C == 0)
        zero_flag_nxt = 1'b1;
    else
        zero_flag_nxt = 1'b0;
        
    negative_flag_nxt = data_C[31];    

end

endmodule
