/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    serializer
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Damian Kuznar
 * DATE:      26.07.2019
 *
 *******************************************************************************/

module mtm_Alu_serializer (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire start,
    input  wire [31:0] data_C,   // serial data input
    input  wire [3:0] flags,
    input  wire err_data,
    input  wire err_crc,
    input  wire err_op,
    output wire sout   // serial data output
);

localparam CRC_POLYNOMIAL = 3'b011;

localparam[1:0] CTL = 2'b01; //control header
localparam[1:0] DAT = 2'b00; //data header
localparam STP = 1'b1;       //stop bit

wire[2:0] errorFlags;
assign errorFlags = {err_data,err_crc,err_op};


reg[54:0] shiftReg,shiftReg_nxt; //parallel in, serial out, shifted left
reg[36:0] crcShift,crcShift_nxt;
reg[5:0] bitCounter,bitCounter_nxt;
reg[2:0] cachedErrorFlags,cachedErrorFlags_nxt;
reg cacheNotEmpty,cacheNotEmpty_nxt;
reg sendingError,sendingError_nxt;

wire sendingData,crcEnable;
assign sendingData = bitCounter != 0;
assign crcEnable = bitCounter > (55 - 37 - 3);


//correct set
wire[54:0] correctFrameSet;
assign correctFrameSet = {DAT,data_C[31:24],STP,
                          DAT,data_C[23:16],STP,
                          DAT,data_C[15:8],STP,
                          DAT,data_C[7:0],STP,
                          CTL,1'b0,flags,3'b000,STP};
                          
wire[36:0] crcDataSet;
assign crcDataSet = {data_C,1'b0,flags};
wire[2:0] crc;

mtm_Alu_crc#(3,CRC_POLYNOMIAL) crc_calc(
    .clk(clk),
    .rst(~sendingData),
    .enable(crcEnable),
    .data_bit(crcShift[36]),
    .crc(crc)
);


//error set
wire[10:0] errorFrameSet;
wire pairity;
assign pairity = ^{1'b1,errorFlags,errorFlags};
assign errorFrameSet = {CTL,1'b1,errorFlags,errorFlags,pairity,STP};
wire insertCrc;
assign insertCrc = ~sendingError && bitCounter == (55 - 37 - 3);

always @* begin
    shiftReg_nxt = {shiftReg[53:0],1'b0};
    crcShift_nxt = {crcShift[35:0],1'b0};
    cacheNotEmpty_nxt = cacheNotEmpty;
    sendingError_nxt = sendingError;
    cachedErrorFlags_nxt = cachedErrorFlags;
    bitCounter_nxt = bitCounter;
    if(start) begin
        if(sendingData) begin
            bitCounter_nxt = bitCounter - 1;
            if(errorFlags != 0) begin
                cacheNotEmpty_nxt = 1;
                cachedErrorFlags_nxt = errorFlags;
            end else begin // this scenario wont happen if alu operates correctly
                
            end
        end else begin
            if(errorFlags != 0) begin //setup error frame
                cacheNotEmpty_nxt = 0;
                sendingError_nxt = 1;
                crcShift_nxt = 0;
                shiftReg_nxt[54:44] = errorFrameSet;//lower bits wont matter
                bitCounter_nxt = 11;//send 11 bits (1 frame)
            end else begin //setup correct frame
                cacheNotEmpty_nxt = 0;
                sendingError_nxt = 0;
                shiftReg_nxt = correctFrameSet;
                crcShift_nxt = crcDataSet;
                bitCounter_nxt = 55;//send 55 bits (5 frames)
            end
        end
    end else if(sendingData) begin
        bitCounter_nxt = bitCounter - 1;
        if(insertCrc) begin
            shiftReg_nxt[44:42] = crc;
        end
    end else begin
        if(cacheNotEmpty) begin //send cached error
            cacheNotEmpty_nxt = 0;
            sendingError_nxt = 1;
            crcShift_nxt = 0;
            shiftReg_nxt[54:44] = errorFrameSet;//lower bits wont matter
            bitCounter_nxt = 11;//send 11 bits (1 frame)
        end else begin //finished sending
            bitCounter_nxt = 0;
        end
    end
end

always @(posedge clk) begin
    if(~rst_n) begin
        shiftReg <= 0;
        bitCounter <= 0;
        cachedErrorFlags <= 0;
        cacheNotEmpty <= 0;
        sendingError <= 0;
        crcShift <= 0;
    end else begin
        shiftReg <= shiftReg_nxt;
        bitCounter <= bitCounter_nxt;
        cachedErrorFlags <= cachedErrorFlags_nxt;
        cacheNotEmpty <= cacheNotEmpty_nxt;
        sendingError <= sendingError_nxt;
        crcShift <= crcShift_nxt;
    end
end

assign sout = shiftReg[54] || ~sendingData;

endmodule
