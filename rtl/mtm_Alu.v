/******************************************************************************
 * (C) Copyright 2019 AGH UST All Rights Reserved
 *
 * MODULE:    mtm_Alu
 * PROJECT:   PPCU_VLSI
 * AUTHORS:   Przemys�aw Paw�ski, Damian Ku�nar
 * DATE:      26.07.2019
 * ------------------------------------------------------------------------------
 * The ALU should operate as described in the mtmAlu_test_top module.
 * It should consist of three modules connected together:
 *   mtm_Alu_deserializer
 *   mtm_Alu_core
 *   mtm_Alu_serializer
 * The ALU should use posedge active clock and synchronous reset active LOW.
 *
 *******************************************************************************/

module mtm_Alu (
    input  wire clk,   // posedge active clock
    input  wire rst_n, // synchronous reset active low
    input  wire sin,   // serial data input
    output wire sout   // serial data output
);

    wire [31:0] data_A;
    wire [31:0] data_B;
    wire [32:0] data_C;
    wire data_ready_1, data_ready_2;
    wire [3:0] flags;
    wire err_op, err_crc_1, err_data_1, err_crc_2, err_data_2;
    wire [2:0] op;

mtm_Alu_deserializer u_mtm_Alu_deserializer(
    .clk(clk),
    .rst_n(rst_n),
    .sin(sin),
    .data_A(data_A),
    .data_B(data_B),
    .op(op), 
    .data_ready(data_ready_1),
    .err_data(err_data_1),
    .err_crc(err_crc_1)
);

mtm_Alu_core u_mtm_Alu_core(
    .clk(clk),
    .rst_n(rst_n),
    .data_A(data_A),
    .data_B(data_B),
    .op(op),
    .start(data_ready_1),
    .err_data_in(err_data_1),
    .err_crc_in(err_crc_1),
    .data_C(data_C),
    .flags(flags),
    .data_ready(data_ready_2),
    .err_op_out(err_op),
    .err_data_out(err_data_2),
    .err_crc_out(err_crc_2)
);

mtm_Alu_serializer u_mtm_Alu_serializer(
    .clk(clk),
    .rst_n(rst_n),
    .start(data_ready_2),
    .data_C(data_C[31:0]),
    .flags(flags),
    .err_data(err_data_2),
    .err_crc(err_crc_2),
    .err_op(err_op),
    .sout(sout) 
);

endmodule
