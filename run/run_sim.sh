#!/bin/bash
# to start simvision add "-gui" option at the command line


source /cad/env/cadence_path.XCELIUM1809
TB_DIR="$(pwd)/../tb"

# xrun replaces irun
xrun \
  +define+TB_DIR="\"$TB_DIR\"" \
  -F mtm_Alu.f \
  +access+r \
  "$@"

# +UVM_VERBOSITY=UVM_DEBUG \
