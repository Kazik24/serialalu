#!/bin/bash
# This script collects interesting data from synthesis, p&r, ant also logs from rtl and postlayout simulations

cp ../synth/genus_mtm.log ./genus_mtm.log
cp ../synth/RESULTS/mtm_Alu.v ./mtm_Alu.v
cp ../pr/innovus_mtm.log ./innovus_mtm.log
cp ../pr/timingReports/12_signoff_time_mtm_Alu.summary ./12_signoff_time_mtm_Alu.summary
cp ../pr/timingReports/14_check_drc.rpt ./14_check_drc.rpt
cp ../pr/timingReports/14_check_connectivity.rpt ./14_check_connectivity.rpt
cp ../pr/timingReports/07_clock_tree_summary.txt ./07_clock_tree_summary.txt
cp ../pr/RESULTS_PR/mtm_Alu.gds.gz ./mtm_Alu.gds.gz
cp ../pr/RESULTS_PR/mtm_Alu.sdf.gz ./mtm_Alu.noPower.v.gz
cp ../pr/RESULTS_PR/mtm_Alu.sdf.gz ./mtm_Alu.sdf.gz
cp ../run/xrun.log ./xrun_rtl.log
cp ../run_post/xrun.log ./xrun_post.log
