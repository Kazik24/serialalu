#!/bin/bash
# This script cleans results folder

rm ./genus_mtm.log
rm ./mtm_Alu.v
rm ./innovus_mtm.log
rm ./12_signoff_time_mtm_Alu.summary
rm ./14_check_drc.rpt
rm ./14_check_connectivity.rpt
rm ./07_clock_tree_summary.txt
rm ./mtm_Alu.gds.gz
rm ./mtm_Alu.noPower.v.gz
rm ./mtm_Alu.sdf.gz
rm ./xrun_rtl.log
rm ./xrun_post.log
